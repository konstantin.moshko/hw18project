﻿#include <iostream>
#include <vector>

using namespace std;

class StackKlass
{
public:
	vector <int> array;

	void randomize(int r)
	{
		for (int i = 0; i < r; i++)
		{
			int q = rand() % 9 + 0;                             //получаем случайное число от 0 до 9
			array.insert(array.end(), q);						//добавляем новый элемент в конец массива, начиная с нулевого
			cout << q << " ";									//печатаем элемент массива
		}
		
	}

	void pop()													//удаляем последний элемент
	{
		array.pop_back();
		cout << "\n" << "последний элемент после удаления: \n" << array.back();
	}

	void push()													//добавляем новый элемент в конец массива
	{
		cout << "\n" << "новый элемент:\n";
		int y;
		cin >> y;
		array.push_back(y);

	}

	void print()												//печатаем весь массив
	{
		cout << "массив:\n";
		for (int i = 0; i < array.size(); i++)
		{
			cout << array[i] << " ";
		}
	}
};

int main()
{
	setlocale(0, "");

	cout << "введи количество ячеек: ";                         //просим ввести количество элементов стэка
	int x;
	cin >> x;                                                   //инициализация и ввод
    StackKlass sc;                                              //создаем обьект класса StackClass
    sc.randomize(x);                                            //передаём количество элементов в метод randomize
	
	sc.push();													//добавляем новый элемент в конец массива
	
	sc.print();													//печатаем весь массив

	sc.pop();													//удаляем последний элемент
	

	return 0;
}










//#include <iostream>
//#include <stack>                                                //библиотека для использования стэка
//
//using namespace std;
//
//class StackClass
//{
//private:
//	int r;                                                      //for randomize
//public:
//    stack <int> steck;                                          //создали связанный список
//
//	void randomize(int r)                                       
//	{
//        for (int i = 1; i <= r; i++)
//        {
//            int q = rand() % 9 + 0;                             //получаем случайное число от 0 до 9
//            steck.push(q);                                      //доваляем число в стэк
//            //cout << q << '\n';                                //печатаем элемент стэка
//        }
//	}
//	
//};
//
//int main()
//{
//	
//	cout << "enter int count: ";                                //просим ввести количество элементов стэка
//	int x;
//	cin >> x;                                                   //инициализация и ввод
//    StackClass sc;                                              //создаем обьект класса StackClass
//    sc.randomize(x);                                            //передаём количество элементов в метод randomize
//    cout << "\n top: " << sc.steck.top() << "\n";               //выводим верхний элемент стэка
//    sc.steck.pop();                                             //удаляем верхний элемент
//    cout << "delete top;\nNew top: " << sc.steck.top();         //выводим новый элемент
//
//	return 0;
//}